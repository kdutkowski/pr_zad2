#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <time.h>

#include "helper.h" 

int n = 9;

int cucumbersCount;
pthread_mutex_t cucumbersLock;
pthread_cond_t cucmbersCond;

int wineCount;
pthread_mutex_t wineLock;
pthread_cond_t wineCond;

int *knightStates;
pthread_mutex_t *stateLocks;
pthread_cond_t *stateConds; 

pthread_mutex_t *speechLocks;
pthread_mutex_t *plates;
pthread_mutex_t *goblets;

void* knight(void*);
void speak(int, int[2]);
void eatAndDrink(int, int[2]);
void getFoodAndWine();
void* waiterWork();
void refill();

void* waiterWork()
{
	int refillTime = getRandomInRange(300000, 1000000);
	while(1)
	{
		usleep(refillTime);
		refill();
	}
	
	return NULL;
}

void refill()
{
	// modyfikuje 2 zmienne warunkowe - wino i ogóry
	printf("\tDOLEWKA\n");
	pthread_mutex_lock(&wineLock);	
	wineCount = n/2;
	pthread_cond_broadcast(&wineCond);
	pthread_mutex_unlock(&wineLock);
	
	pthread_mutex_lock(&cucumbersLock);
	cucumbersCount = n/3;
	pthread_cond_broadcast(&cucmbersCond);
	pthread_mutex_unlock(&cucumbersLock);	
}

void* knight(void* num)
{
	int id = ((int)num);
	
	
	//	numery sąsiadów przy stole (z zawijaniem stołu
	int neighbors[2];
	neighbors[0] = id ? id - 1 : n;
	neighbors[1] = id == n ? 0 : id + 1;
	
	// 10 kolejek picia i mówienia z losową kolejnością 
	// (żeby nie wszyscy zaczynali w tej samej kolejności
	int i;
	for (i=0; i<10; i++)
	{
		//if(getRandomInRange(0, 10) % 2 == 0)
		//{
			speak(id, neighbors);
			eatAndDrink(id, neighbors);
		/*}
		else
		{
			eatAndDrink(id, neighbors);
			speak(id, neighbors);
		}*/
	}
	
	printf("rycerz %d kończy zabawę\n", id);
	
	return NULL;
}

void eatAndDrink(int num, int neighbors[2])
{
	// obliczenie odpowiednich numerów talerza i kielicha
	int gobletNumber = num/2;
	int plateNumber = num % 2 == 0 ? num / 2 : num / 2 + 1;
	
	if (num == n)
		plateNumber = 0;
		
	// printf("nr %d kielich %d talerz %d \n", num, gobletNumber, plateNumber);
	
	// w pętli próbuję zablokować ĸielich i talerz na raz, 
	// z randomem, żeby nie było zawsze w tej samej kolejności
	if (num % 2)
	{
		pthread_mutex_lock(&goblets[gobletNumber]);
		pthread_mutex_lock(&plates[plateNumber]);
	}
	else
	{
		pthread_mutex_lock(&plates[plateNumber]);
		pthread_mutex_lock(&goblets[gobletNumber]);
	}
	
	// sprawdzenie, czy sąsiedzi nie mówią 
	pthread_mutex_lock(&stateLocks[neighbors[0]]);
	pthread_mutex_lock(&stateLocks[neighbors[1]]);
	while (knightStates[neighbors[0]] == SPEAKING)
		pthread_cond_wait(&stateConds[neighbors[0]], &stateLocks[neighbors[0]]);			
	
	while (knightStates[neighbors[1]] == SPEAKING)
		pthread_cond_wait(&stateConds[neighbors[1]], &stateLocks[neighbors[1]]);			
	pthread_mutex_unlock(&stateLocks[neighbors[1]]);
	pthread_mutex_unlock(&stateLocks[neighbors[0]]);


	if(num != KING_NO)
	{
		// sprawdzenie, czy król nie mówi
		pthread_mutex_lock(&stateLocks[KING_NO]);
		while (knightStates[KING_NO] == SPEAKING)
			pthread_cond_wait(&stateConds[KING_NO], &stateLocks[KING_NO]);			
		pthread_mutex_unlock(&stateLocks[KING_NO]);
	}
	
	getFoodAndWine();
	
	printf("rycerz %d je i pije\n", num);
	
	pthread_mutex_unlock(&goblets[gobletNumber]);
	pthread_mutex_unlock(&plates[plateNumber]);
}

void speak(int num, int neighbors[2])
{
	// printf("rycerz nr %d chce opowiadać\n", num);	
	//fprintf(stderr, "rycerz nr %d sąsiedzi: %d %d\n", num, neighbors[0], neighbors[1]);
	
	if (num % 2)
	{
		pthread_mutex_lock(&speechLocks[num]);
		pthread_mutex_lock(&speechLocks[(num + 1) % (n + 1)]);
	}
	else
	{
		pthread_mutex_lock(&speechLocks[(num + 1) % (n + 1)]);
		pthread_mutex_lock(&speechLocks[num]);
	}
	
	if(num == KING_NO)
	{
		printf("KRÓL MÓWI\n");	
	}
	else
	{
		// czekanie, aż król skończy mówić (jeśli mówił)
		pthread_mutex_lock(&stateLocks[KING_NO]);
		while (knightStates[KING_NO] == SPEAKING)
			pthread_cond_wait(&stateConds[KING_NO], &stateLocks[KING_NO]);			
		pthread_mutex_unlock(&stateLocks[KING_NO]);
	}
	
	//	ustawiam swój stan na SPEAKING
	pthread_mutex_lock(&stateLocks[num]);
	knightStates[num] = SPEAKING;
	pthread_cond_broadcast(&stateConds[num]);
	pthread_mutex_unlock(&stateLocks[num]);

	int speechTime = getRandomInRange(100, 500);
	
	printf("rycerz nr %d zaczyna opowiadać przez %d ms\n", num, speechTime);
	
	usleep(speechTime * 1000);
	
	printf("rycerz nr %d kończy opowiadać\n", num);
	
	pthread_mutex_lock(&stateLocks[num]);
	knightStates[num] = NOT_SPEAKING;
	pthread_cond_broadcast(&stateConds[num]);
	pthread_mutex_unlock(&stateLocks[num]);
	
	pthread_mutex_unlock(&speechLocks[(num + 1) % (n + 1)]);
	pthread_mutex_unlock(&speechLocks[num]);
}


void getFoodAndWine()
{
	pthread_mutex_lock(&wineLock);
	while (wineCount < 1)
		pthread_cond_wait(&wineCond, &wineLock);			
	wineCount--;
	//printf("\tzostało %d win\n", wineCount);
	pthread_mutex_unlock(&wineLock);
	
	pthread_mutex_lock(&cucumbersLock);
	while (cucumbersCount < 1)
		pthread_cond_wait(&cucmbersCond, &cucumbersLock);
	cucumbersCount--;
	//printf("\tzostało %d ogórków\n", cucumbersCount);
	pthread_mutex_unlock(&cucumbersLock);
}


int main(int argc, char** argv)
{
	if (argc == 2)
		n = atoi(argv[1]);
	
	pthread_t *knights;
	pthread_t waiter;
	
	
	//	rycerz o numerze 0 to król, stąd n+1 rycerzy
	knights = (pthread_t*)malloc((n + 1) * sizeof(pthread_t));	
		
	//	zminne warunkowe - dzban wina i misa z ogórkami	
	cucumbersCount = n/3;
	wineCount = n/2;
	pthread_mutex_init(&cucumbersLock, NULL);
	pthread_cond_init(&cucmbersCond, NULL);
	
	pthread_mutex_init(&wineLock, NULL);
	pthread_cond_init(&wineCond, NULL);
	
	
	//	zmienna warunkowa - tablica stanów (SPEAKING/NOT_SPEAKING)
	knightStates = (int*)malloc((n + 1) * sizeof(int));
	stateLocks = (pthread_mutex_t*)malloc((n + 1) * sizeof(pthread_mutex_t));
	stateConds = (pthread_cond_t*)malloc((n + 1) * sizeof(pthread_cond_t));
	
	//	mutexy dla każdego rycerza - jak mówi to blokuje
	speechLocks = (pthread_mutex_t*)malloc((n + 1) * sizeof(pthread_mutex_t));
	
	//	mutexy dla talerzy i kielichów
	plates = (pthread_mutex_t*)malloc((n/2 + 1) * sizeof(pthread_mutex_t));
	goblets = (pthread_mutex_t*)malloc((n/2 + 1) * sizeof(pthread_mutex_t));
	

	
	int i;
	for (i = 0; i < n + 1; i++)
		knightStates[i] = NOT_SPEAKING;
    
    for (i = 0; i < n + 1; i++)
		pthread_mutex_init (&speechLocks[i], NULL);	
	for (i = 0; i < n + 1; i++)
		pthread_mutex_init (&stateLocks[i], NULL);
	for (i = 0; i < n + 1; i++)
		pthread_cond_init (&stateConds[i], NULL);		
    
    pthread_create (&waiter, NULL, waiterWork, NULL);
    for (i = 0; i < n + 1; i++)
		pthread_create (&knights[i], NULL, knight, ((void *)i));
	for (i = 0; i < n + 1; i++)
		pthread_join (knights[i], NULL);	
	
	pthread_cancel(waiter);		   	
			   	
	for (i = 0; i < n + 1; i++)
		pthread_mutex_destroy (&speechLocks[i]);	
	for (i = 0; i < n + 1; i++)
		pthread_mutex_destroy (&stateLocks[i]);	
	for (i = 0; i < n + 1; i++)
		pthread_cond_destroy (&stateConds[i]);	
		
	pthread_mutex_destroy (&cucumbersLock);   
	pthread_mutex_destroy (&wineLock);  	
	
	pthread_cond_destroy (&wineCond);
	pthread_cond_destroy (&cucmbersCond);
			   	
	free(speechLocks);
	free(plates);
	free(goblets);
	free(knights);
	free(stateConds);
	free(stateLocks);
	free(knightStates);
	
	return EXIT_SUCCESS;
}
