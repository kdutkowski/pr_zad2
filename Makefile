ifeq (run,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

all: compile

compile: knights

knights: knights.o 
	gcc -o knights knights.o -lpthread

knights.o: knights.c
	gcc -Wall -c knights.c

run:
	./knights $(RUN_ARGS)

test: knights
	./knights < input.txt > output.txt

zip: 
	mkdir dutkowskij
	cp -t dutkowskij *.h *.c Makefile 
	tar -zcvf dutkowskij.tar.gz dutkowskij 

clean:
	rm -f *.o *.gz.c 
