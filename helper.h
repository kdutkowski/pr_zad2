#ifndef _HELPERS_H_
#define _HELPERS_H_

#define RIGHT (i+1)%2
#define LEFT i%2

#define SPEAKING 1
#define NOT_SPEAKING 0
#define KING_NO 0

int getRandomInRange(int min, int max)
{
	unsigned int seed = time(NULL) * getpid() * (pthread_self() + 1);
	return rand_r(&seed) % (max - min + 1) + min; 
}


#endif
